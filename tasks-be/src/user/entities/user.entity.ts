import { Column, Entity } from "typeorm";
import { BaseEntityProperties } from "../../common/entities/base-entity-properties";

@Entity()
export class User extends BaseEntityProperties {
  @Column({ unique: true, nullable: false })
  email: string;

  @Column({ nullable: false })
  password: string;
}
