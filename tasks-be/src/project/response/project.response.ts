import { AutoMap } from "@automapper/classes";
import { ApiProperty } from "@nestjs/swagger";

export class ProjectResponse {
  @AutoMap()
  @ApiProperty({ type: 'integer' })
  id: number;

  @AutoMap()
  @ApiProperty()
  name: string;

  @AutoMap()
  @ApiProperty()
  description: string;

  @AutoMap()
  @ApiProperty({ format: 'date-time' })
  createdAt: Date;

  @AutoMap()
  @ApiProperty({ format: 'date-time' })
  updatedAt: Date;
}
