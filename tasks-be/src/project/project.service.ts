import { Repository } from "typeorm";
import { Injectable, NotFoundException } from "@nestjs/common";
import { applyQuery } from 'typeorm-extension';
import { Mapper } from "@automapper/core";
import { InjectMapper } from "@automapper/nestjs";
import { InjectRepository } from "@nestjs/typeorm";
import { Project } from "./entities/project.entity";
import { MAX_LIMIT } from "../common/config/api.config";
import { ProjectResponse } from "./response/project.response";
import { CreateProjectRequest } from "./request/create-project.request";
import { UpdateProjectRequest } from "./request/update-project.request";

@Injectable()
export class ProjectService {
  constructor(
    @InjectRepository(Project)
    private readonly projectRepository: Repository<Project>,
    @InjectMapper() private readonly mapper: Mapper,
  ) {}

  async create(request: CreateProjectRequest) {
    const safeRequest = this.mapper.map(request, CreateProjectRequest, Project);
    const project = await this.projectRepository.create(safeRequest);
    return this.mapper.map(
      await this.projectRepository.save(project),
      Project,
      ProjectResponse
    );
  }

  async paginate(requestQuery: object) {
    const queryBuilder = this.projectRepository.createQueryBuilder('project');

    const { pagination } = applyQuery(queryBuilder, requestQuery as unknown, {
      defaultAlias: 'project',
      filters: {
        allowed: ['id', 'name', 'description', 'createdAt', 'updatedAt'],
      },
      pagination: {
        maxLimit: MAX_LIMIT,
      },
      sort: {
        allowed: ['id', 'name', 'description', 'createdAt', 'updatedAt'],
        default: {
          createdAt: 'DESC',
        },
      }
    });

    const [projects, total] = await queryBuilder.getManyAndCount();
    return {
      data: this.mapper.mapArray(projects, Project, ProjectResponse),
      meta: {
        total,
        ...pagination,
      },
    }
  }

  async findOne(id: number) {
    return this.mapper.map(
      await this.getById(id),
      Project,
      ProjectResponse
    );
  }

  async update(id: number, request: UpdateProjectRequest) {
    const project = await this.getById(id);
    const safeRequest = this.mapper.map(request, UpdateProjectRequest, Project);
    const updatedProject = Object.assign(project, safeRequest);
    return this.mapper.map(
      await this.projectRepository.save(updatedProject),
      Project,
      ProjectResponse
    );
  }

  async remove(id: string): Promise<void> {
    await this.projectRepository.delete(id);
  }

  private async getById(id: number): Promise<Project> {
    const project = await this.projectRepository.findOneBy({ id });
    if (!project) {
      throw new NotFoundException('Project not found');
    }
    return project;
  }
}
