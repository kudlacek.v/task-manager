import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Project } from "./entities/project.entity";
import { ProjectController } from "./project.controller";
import { ProjectService } from "./project.service";
import { ProjectMapper } from "./mapper/project.mapper";

@Module({
  imports: [TypeOrmModule.forFeature([Project])],
  controllers: [ProjectController],
  providers: [ProjectService, ProjectMapper],
  exports: [TypeOrmModule]
})
export class ProjectModule {}
