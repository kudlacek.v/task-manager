import { Column, Entity, OneToMany } from "typeorm";
import { AutoMap } from "@automapper/classes";
import { Task } from "../../task/entities/task.entity";
import { BaseEntityProperties } from "../../common/entities/base-entity-properties";

@Entity()
export class Project extends BaseEntityProperties {
  @AutoMap()
  @Column({ nullable: false })
  name: string;

  @AutoMap()
  @Column({ nullable: false })
  description: string;

  @AutoMap()
  @Column({ name: 'start_date', nullable: false, type: 'timestamptz' })
  startDate: Date;

  @AutoMap()
  @Column({ name: 'end_date', nullable: true, type: 'timestamptz' })
  endDate: Date;

  @OneToMany(() => Task, task => task.projectId)
  tasks: Task[];
}
