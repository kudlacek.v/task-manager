import { AutoMap } from "@automapper/classes";
import { ApiProperty } from "@nestjs/swagger";
import { IsDateString, IsNotEmpty } from "class-validator";

export class CreateProjectRequest {
  @AutoMap()
  @ApiProperty()
  @IsNotEmpty()
  name: string;

  @AutoMap()
  @ApiProperty()
  @IsNotEmpty()
  description: string;

  @AutoMap()
  @ApiProperty({ format: 'date-time' })
  @IsDateString()
  startDate: string;

  @AutoMap()
  @ApiProperty({ format: 'date-time' })
  @IsDateString()
  endDate: Date;
}
