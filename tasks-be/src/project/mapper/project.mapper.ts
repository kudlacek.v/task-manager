import { Injectable } from "@nestjs/common";
import { Mapper, MappingProfile, createMap } from "@automapper/core";
import { AutomapperProfile, InjectMapper } from "@automapper/nestjs";
import { Project } from "../entities/project.entity";
import { ProjectResponse } from "../response/project.response";
import { UpdateProjectRequest } from "../request/update-project.request";
import { CreateProjectRequest } from "../request/create-project.request";

@Injectable()
export class ProjectMapper extends AutomapperProfile {
  constructor(@InjectMapper() mapper: Mapper) {
    super(mapper);
  }

  override get profile(): MappingProfile {
    return (mapper: Mapper) => {
      createMap(mapper, Project, ProjectResponse);
      createMap(mapper, UpdateProjectRequest, Project);
      createMap(mapper, CreateProjectRequest, Project);
    }
  }
}
