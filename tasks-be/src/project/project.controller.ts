import { Request } from 'express';
import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, Req } from "@nestjs/common";
import { ApiCreatedResponse, ApiNoContentResponse, ApiOkResponse, ApiOperation } from "@nestjs/swagger";
import { ApiOkResponsePaginated } from '../common/swagger/api-ok-response-paginated';
import { ProjectService } from "./project.service";
import { ProjectResponse } from './response/project.response';
import { CreateProjectRequest } from './request/create-project.request';
import { UpdateProjectRequest } from './request/update-project.request';

@Controller('project')
export class ProjectController {
  constructor(private readonly projectService: ProjectService) {}

  @ApiOperation({ summary: 'Create project' })
  @ApiCreatedResponse({ type: ProjectResponse })
  @Post()
  async create(@Body() request: CreateProjectRequest) {
    return await this.projectService.create(request);
  }

  @ApiOperation({ summary: 'Get projects filtered and paginated' }) // TODO: comment how filters and pagination work
  @ApiOkResponsePaginated(ProjectResponse)
  @Get()
  async paginate(
    @Req() request: Request,
  ) {
    return await this.projectService.paginate(request.query);
  }

  @ApiOperation({ summary: 'Get project by id' })
  @ApiOkResponse({ type: ProjectResponse })
  @Get(':id')
  async findOne(@Param('id') id: number) {
    return await this.projectService.findOne(id);
  }

  @ApiOperation({ summary: 'Update project by id' })
  @ApiOkResponse({ type: ProjectResponse })
  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() request: UpdateProjectRequest,
  ) {
    return await this.projectService.update(id, request);
  }

  @ApiOperation({ summary: 'Delete project by id' })
  @ApiNoContentResponse()
  @Delete(':id')
  @HttpCode(204)
  async remove(@Param('id') id: string) {
    await this.projectService.remove(id);
  }
}
