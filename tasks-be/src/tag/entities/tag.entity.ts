import { Column, Entity } from "typeorm";
import { BaseEntityProperties } from "../../common/entities/base-entity-properties";

@Entity()
export class Tag extends BaseEntityProperties {
  @Column({ nullable: false })
  name: string;
}
