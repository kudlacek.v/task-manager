import { Body, Controller, Delete, HttpCode, Param, Post } from "@nestjs/common";
import { ApiCreatedResponse, ApiNoContentResponse, ApiOperation } from "@nestjs/swagger";
import { TagService } from "./tag.service";
import { TagResponse } from "./response/tag.response";
import { CreateTagRequest } from "./request/create-tag.request";

@Controller('tag')
export class TagController {
  constructor(private readonly tagService: TagService) {}

  @ApiOperation({ summary: 'Create tag' })
  @ApiCreatedResponse({ type: TagResponse })
  @Post()
  async create(@Body() request: CreateTagRequest) {
    return await this.tagService.create(request);
  }

  @ApiOperation({ summary: 'Delete tag by id' })
  @ApiNoContentResponse()
  @Delete(':id')
  @HttpCode(204)
  async remove(@Param('id') id: string) {
    await this.tagService.remove(id);
  }
}
