import { InjectMapper } from "@automapper/nestjs";
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Tag } from "./entities/tag.entity";
import { Repository } from "typeorm";
import { Mapper } from "@automapper/core";
import { CreateTagRequest } from "./request/create-tag.request";
import { TagResponse } from "./response/tag.response";

@Injectable()
export class TagService {
  constructor(
    @InjectRepository(Tag)
    private readonly tagRepository: Repository<Tag>,
    @InjectMapper()
    private readonly mapper: Mapper
  ) {}

  async create(request: object) {
    const safeRequest = this.mapper.map(request, CreateTagRequest, Tag);
    const tag = await this.tagRepository.create(safeRequest);
    return this.mapper.map(
      await this.tagRepository.save(tag),
      Tag,
      TagResponse
    );
  }

  async remove(id: string): Promise<void> {
    await this.tagRepository.delete(id);
  }
}
