import { AutoMap } from "@automapper/classes";
import { ApiProperty } from "@nestjs/swagger";

export class TagResponse {
  @AutoMap()
  @ApiProperty({ type: 'integer' })
  id: number;

  @AutoMap()
  @ApiProperty()
  name: string;
}
