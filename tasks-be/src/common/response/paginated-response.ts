import { ApiProperty } from '@nestjs/swagger';

export class PaginatedResponseMeta {
  @ApiProperty({ type: Number })
  total: number;

  @ApiProperty({ type: Number })
  limit: number;

  @ApiProperty({ type: Number })
  offset: number;
}

export class PagginatedResponse<TItem> {
  @ApiProperty({ type: [Object], isArray: true }) // correct type is mapped by ApiOkResponsePaginated decorator
  data: TItem[];

  @ApiProperty()
  meta: PaginatedResponseMeta;
}

