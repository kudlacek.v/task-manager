import * as fs from 'fs';
import * as dotenv from 'dotenv';
import * as dotenvExpand from 'dotenv-expand';
import { getEnvFiles } from './get-env-files';

export function loadEnvFilesIntoProcess() {
  const envFilesToLoad = getEnvFiles().filter(envFile => fs.existsSync(envFile));
  let config = {};
  envFilesToLoad.forEach(envFile => {
    const fileContent = fs.readFileSync(envFile);
    config = {...config, ...dotenv.parse(fileContent) };
  });
  dotenvExpand.expand({ parsed: config });
}
