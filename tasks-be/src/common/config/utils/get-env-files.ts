export function getEnvFiles() {
  const path = `${process.cwd()}/tasks-be`;
  return [`${path}/.env`, `${path}/.env.local`];
}
