import { registerAs } from "@nestjs/config";
import { DataSource, DataSourceOptions } from "typeorm";
import { DatabaseNamingStrategy } from "./database-naming-strategy";
import { loadEnvFilesIntoProcess } from "./utils/load-env-files-into-process";

export const DATABASE_CONFIG_KEY = 'database';

export function getTypeOrmConfig(): DataSourceOptions {
  return {
    type: 'postgres',
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT!),
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    entities: [`${__dirname}/../../../src/**/*.entity.{ts,js}`],
    migrations: [`${__dirname}/../../../migrations/*.{ts,js}`],
    // entities: [`${__dirname}/src/**/*.entity.{ts,js}`],
    // migrations: [`${__dirname}/migrations/*.{ts,js}`],
    schema: process.env.DB_SCHEMA,
    uuidExtension: 'pgcrypto',
    logging: true,
    namingStrategy: new DatabaseNamingStrategy(),
    synchronize: false,
  };
}

export const databaseConfigRegistrator = registerAs(DATABASE_CONFIG_KEY, () => {
  return getTypeOrmConfig();
});

function createDataSource() {
  loadEnvFilesIntoProcess();
  return new DataSource(getTypeOrmConfig());
}

export default createDataSource();
