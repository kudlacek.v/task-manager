import { DefaultNamingStrategy, Table } from "typeorm";

export class DatabaseNamingStrategy extends DefaultNamingStrategy {
  constructor() {
    super();
  }

  primaryKeyName(tableOrName: string | Table, columnNames: string[]): string {
    return `PK_${getTableName(tableOrName)}_${prefixIfExistsArray(columnNames)}`;
  }

  foreignKeyName(tableOrName: string | Table, columnNames: string[], referencedTablePath?: string, referencedColumnNames?: string[]): string {
    return `FK_${this.getTableName(tableOrName)}_${referencedTablePath}_${prefixIfExistsArray(referencedColumnNames)}`;
  }

  indexName(tableOrName: string | Table, columnNames: string[]): string {
    return `IDX_${getTableName(tableOrName)}_${prefixIfExistsArray(columnNames)}`;
  }

  uniqueConstraintName(tableOrName: string | Table, columnNames: string[]): string {
    return `UQ_${getTableName(tableOrName)}_${prefixIfExistsArray(columnNames)}`;
  }

  relationConstraintName(tableOrName: string | Table, columnNames: string[]): string {
    return `REL_${getTableName(tableOrName)}_${prefixIfExistsArray(columnNames)}`;
  }

  checkConstraintName(tableOrName: string | Table, expression: string): string {
    return `CK_${getTableName(tableOrName)}_${expression}`;
  }

  defaultConstraintName(tableOrName: string | Table, columnName: string): string {
    return `DF_${getTableName(tableOrName)}_${columnName}`;
  }

  exclusionConstraintName(tableOrName: string | Table, expression: string): string {
    return `EXCL_${getTableName(tableOrName)}_${expression}`;
  }
}

function getTableName(tableOrName: Table | string): string {
  return typeof tableOrName === 'string' ? tableOrName : tableOrName.name;
}

function prefixIfExistsArray(names?: string[]) {
  return names ? `${names.join('_')}` : '';
}
