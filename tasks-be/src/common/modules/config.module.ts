import { Module } from "@nestjs/common";
import { ConfigModule as NestConfigModule } from '@nestjs/config';
import { databaseConfigRegistrator } from "../config/database.config";
import { getEnvFiles } from "../config/utils/get-env-files";

@Module({
  imports: [
    NestConfigModule.forRoot({
      isGlobal: true,
      envFilePath: getEnvFiles(),
      ignoreEnvFile: false,
      ignoreEnvVars: false,
      load: [databaseConfigRegistrator]
    })
  ]
})
export class ConfigModule {};
