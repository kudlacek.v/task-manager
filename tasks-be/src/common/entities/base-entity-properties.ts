import { AutoMap } from "@automapper/classes";
import { CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

export abstract class BaseEntityProperties {
  @AutoMap()
  @PrimaryGeneratedColumn()
  id: number;

  @AutoMap()
  @CreateDateColumn({ name: "created_at", type: 'timestamptz' })
  createdAt: Date;

  @AutoMap()
  @UpdateDateColumn({ name: "updated_at", type: 'timestamptz' })
  updatedAt: Date;
}
