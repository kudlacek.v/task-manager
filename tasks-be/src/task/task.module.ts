import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Task } from "./entities/task.entity";
import { TaskController } from "./task.controller";
import { TaskService } from "./task.service";
import { TaskMapper } from "./mapper/task.mapper";

@Module({
  imports: [TypeOrmModule.forFeature([Task])],
  controllers: [TaskController],
  providers: [TaskService, TaskMapper],
  exports: [TypeOrmModule],
})
export class TaskModule {}
