import { Injectable } from "@nestjs/common";
import { Mapper, MappingProfile, createMap } from "@automapper/core";
import { AutomapperProfile, InjectMapper } from "@automapper/nestjs";
import { UpdateTaskRequest } from "../request/update-task.request";
import { TaskResponse } from "../response/task.response";
import { Task } from "../entities/task.entity";
import { CreateTaskRequest } from "../request/create-task.request";

@Injectable()
export class TaskMapper extends AutomapperProfile {
  constructor(@InjectMapper() mapper: Mapper) {
    super(mapper);
  }

  override get profile(): MappingProfile {
    return (mapper: Mapper) => {
      createMap(mapper, Task, TaskResponse);
      createMap(mapper, UpdateTaskRequest, Task);
      createMap(mapper, CreateTaskRequest, Task);
    }
  }
}
