import { AutoMap } from "@automapper/classes";
import { ApiProperty } from "@nestjs/swagger";

export class TaskResponse {
  @AutoMap()
  @ApiProperty({ type: 'integer' })
  id: number;

  @AutoMap()
  @ApiProperty()
  name: string;

  @AutoMap()
  @ApiProperty()
  description: string;

  @AutoMap()
  @ApiProperty({ type: 'string', enum: ['NEW', 'IN_PROGRESS', 'DONE'] })
  state: string;

  @AutoMap()
  @ApiProperty({ type: Number })
  projectId: number;

  @AutoMap()
  @ApiProperty()
  tags: string[];
}
