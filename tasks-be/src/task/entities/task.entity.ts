import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne } from "typeorm";
import { AutoMap } from "@automapper/classes";
import { BaseEntityProperties } from "../../common/entities/base-entity-properties";
import { Tag } from "../../tag/entities/tag.entity";
import { Project } from "../../project/entities/project.entity";

export enum TaskState {
  NEW = 'NEW',
  IN_PROGRESS = 'IN_PROGRESS',
  DONE = 'DONE',
}

@Entity()
export class Task extends BaseEntityProperties {
  @AutoMap()
  @Column({ nullable: false })
  name: string;

  @AutoMap()
  @Column({ nullable: false })
  description: string;

  @AutoMap()
  @Column({ enum: TaskState, nullable: false, default: TaskState.NEW })
  state: TaskState;

  @ManyToOne(() => Project, project => project.tasks)
  @JoinColumn({ name: 'project_id' })
  projectId: number;

  @ManyToMany(() => Tag)
  @JoinTable()
  tags: Tag[];
}
