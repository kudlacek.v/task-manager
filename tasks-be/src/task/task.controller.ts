import { Request } from 'express';
import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, Req } from "@nestjs/common";
import { TaskService } from "./task.service";
import { ApiCreatedResponse, ApiNoContentResponse, ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import { ApiOkResponsePaginated } from '../common/swagger/api-ok-response-paginated';
import { TaskResponse } from './response/task.response';
import { CreateTaskRequest } from './request/create-task.request';
import { UpdateTaskRequest } from './request/update-task.request';

@Controller('task')
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @ApiOperation({ summary: 'Create task' })
  @ApiCreatedResponse({ type: TaskResponse })
  @Post()
  async create(@Body() request: CreateTaskRequest) {
    return await this.taskService.create(request);
  }

  @ApiOperation({ summary: 'Get tasks filtered and paginated' }) // TODO: comment how filters and pagination work
  @ApiOkResponsePaginated(TaskResponse)
  @Get()
  async paginate(@Req() request: Request) {
    return await this.taskService.paginate(request.query);
  }

  @ApiOperation({ summary: 'Get task by id' })
  @ApiOkResponse({ type: TaskResponse })
  @Get(':id')
  async findOne(@Param('id') id: number) {
    return await this.taskService.findOne(id);
  }

  @ApiOperation({ summary: 'Update task by id' })
  @ApiOkResponse({ type: TaskResponse })
  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() request: UpdateTaskRequest,
  ) {
    return await this.taskService.update(id, request);
  }

  @ApiOperation({ summary: 'Delete task by id' })
  @ApiNoContentResponse()
  @Delete(':id')
  @HttpCode(204)
  async remove(@Param('id') id: string) {
    await this.taskService.remove(id);
  }
}
