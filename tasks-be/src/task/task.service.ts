import { Injectable, NotFoundException } from "@nestjs/common";
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { InjectMapper } from "@automapper/nestjs";
import { Mapper } from "@automapper/core";
import { MAX_LIMIT } from "../common/config/api.config";
import { Task } from "./entities/task.entity";
import { applyQuery } from "typeorm-extension";
import { TaskResponse } from "./response/task.response";
import { CreateTaskRequest } from "./request/create-task.request";
import { UpdateTaskRequest } from "./request/update-task.request";

@Injectable()
export class TaskService {
  constructor(
    @InjectRepository(Task)
    private readonly taskRepository: Repository<Task>,
    @InjectMapper()
    private readonly mapper: Mapper
  ) {}

  async create(request: CreateTaskRequest) {
    const safeRequest = this.mapper.map(request, CreateTaskRequest, Task);
    const task = await this.taskRepository.create(safeRequest);
    return this.mapper.map(
      await this.taskRepository.save(task),
      Task,
      TaskResponse
    );
  }

  async paginate(requestQuery: object) {
    const queryBuilder = this.taskRepository.createQueryBuilder("task");

    const { pagination } = applyQuery(queryBuilder, requestQuery as unknown, {
      defaultAlias: "task",
      filters: {
        allowed: ["id", "name", "description", "projectId", "createdAt", "updatedAt"],
      },
      pagination: {
        maxLimit: MAX_LIMIT,
      },
      sort: {
        allowed: ["id", "name", "description", "projectId", "createdAt", "updatedAt"],
        default: {
          createdAt: "DESC",
        },
      },
    });

    const [tasks, total] = await queryBuilder.getManyAndCount();
    return {
      data: this.mapper.mapArray(tasks, Task, TaskResponse),
      meta: {
        total,
        ...pagination,
      },
    };
  }

  async findOne(id: number) {
    return this.mapper.map(
      await this.getById(id),
      Task,
      TaskResponse
    );
  }

  async update(id: number, request: UpdateTaskRequest) {
    const task = await this.getById(id);
    const safeRequest = this.mapper.map(request, UpdateTaskRequest, Task);
    const updatedTask = Object.assign(task, safeRequest);
    return this.mapper.map(
      await this.taskRepository.save(updatedTask),
      Task,
      TaskResponse
    );
  }

  async remove(id: string): Promise<void> {
    await this.taskRepository.delete(id);
  }

  private async getById(id: number): Promise<Task> {
    const task = await this.taskRepository.findOneBy({ id });
    if (!task) {
      throw new NotFoundException('Task not found');
    }
    return task;
  }
}
