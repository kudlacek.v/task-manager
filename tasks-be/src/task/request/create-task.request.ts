import { AutoMap } from "@automapper/classes";
import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class CreateTaskRequest {
  @AutoMap()
  @ApiProperty()
  @IsNotEmpty()
  name: string;

  @AutoMap()
  @ApiProperty()
  @IsNotEmpty()
  description: string;

  @AutoMap()
  @ApiProperty({ type: Number })
  @IsNotEmpty()
  projectId: number;

  @AutoMap()
  @ApiProperty({ type: Number, isArray: true })
  @IsNotEmpty()
  tags: number[];
}
