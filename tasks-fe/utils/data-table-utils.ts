import { SortBuildInput } from 'rapiq';

export function toTableSortField<TModel extends Record<string, unknown>>(
  sort?: SortBuildInput<TModel>
) {
  if (!sort) {
    return undefined;
  }

  if (typeof sort === 'string') {
    return (sort as string).replace(/^-/, '');
  }

  throw new Error('Not implemented');
}

export function toTableSortOrder<TModel extends Record<string, unknown>>(
  sort?: SortBuildInput<TModel>
) {
  if (!sort) {
    return undefined;
  }

  if (typeof sort === 'string') {
    return (sort as string).startsWith('-') ? -1 : 1;
  }

  throw new Error('Not implemented');
}
