'use client';

import { FC } from 'react';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { Menu } from 'primereact/menu';
import { Badge } from 'primereact/badge';
import { MenuItem } from 'primereact/menuitem';
import { classNames } from "primereact/utils";


type MyMenuItem = MenuItem & {
  badge?: string;
  shortcut?: string;
  href?: string;
};

export const MainMenu: FC = () => {
  const currentRoute = usePathname();

  const itemRenderer = (item: MyMenuItem) => (
    <div className={classNames("p-menuitem-content", { "bg-gray-400": currentRoute === item.href })}>
      <Link
        href={item.href ?? '#'}
        className="flex align-items-center p-menuitem-link"
      >
        <span className={item.icon} />
        <span className="mx-2">{item.label}</span>
        {item.badge && <Badge className="ml-auto" value={item.badge} />}
        {item.shortcut && (
          <span className="ml-auto border-1 surface-border border-round surface-100 text-xs p-1">
            {item.shortcut}
          </span>
        )}
      </Link>
    </div>
  );

  const items: MyMenuItem[] = [
    {
      label: 'Tasks',
      items: [
        {
          label: 'My Tasks TODO:',
          icon: 'pi pi-user',
          href: '/tasks/my',
          template: itemRenderer,
        },
        {
          label: 'List',
          icon: 'pi pi-list',
          href: '/tasks',
          template: itemRenderer,
        },
        {
          label: 'New',
          icon: 'pi pi-plus-circle',
          href: '/tasks/new',
          template: itemRenderer,
        },
      ] as MyMenuItem[],
    },
    {
      separator: true,
    },
    {
      label: 'Projects',
      items: [
        {
          label: 'List',
          icon: 'pi pi-list',
          href: '/projects',
          template: itemRenderer,
        },
        {
          label: 'New',
          icon: 'pi pi-plus-circle',
          href: '/projects/new',
          template: itemRenderer,
        },
      ] as MyMenuItem[],
    },
    {
      separator: true,
    },
    {
      label: 'Settings',
      icon: 'pi pi-cog',
      template: itemRenderer,
    },
    {
      separator: true,
    },
    {
      label: 'Logout',
      icon: 'pi pi-sign-out',
      template: itemRenderer,
    },
  ];

  return <Menu model={items} autoFocus={false} />;
};
