import { FC, ReactNode } from "react";
import { twMerge } from "tailwind-merge";

type Props = {
  children: ReactNode;
  className?: string;
}

export const BlockTitle: FC<Props> = ({ children, className }) => {
  return (
    <div className={twMerge("bg-gray-700 text-white py-3 pl-4 rounded-md", className)}>
      {children}
    </div>
  );
}
