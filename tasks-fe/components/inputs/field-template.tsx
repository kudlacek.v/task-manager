import { FC, ReactNode } from "react";
import { classNames } from "primereact/utils";

type Props = {
  name: string;
  label: string;
  isInvalid: boolean;
  errorMessage?: string;
  children: ReactNode;
}

export const FieldTemplate: FC<Props> = ({ name, label, isInvalid, errorMessage, children }) => {
  return (
    <div className="p-2 flex flex-col">
      <label htmlFor={name} className={classNames({ 'p-error': isInvalid })}>{label}</label>
      {children}
      <small className="p-error">{errorMessage as string}</small>
    </div>
  )
}
