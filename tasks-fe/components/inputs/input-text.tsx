import { InputText as PrimeInputText } from "primereact/inputtext";
import { FormInputProps } from "./form-input-props";
import { classNames } from "primereact/utils";
import { FieldTemplate } from "./field-template";

export const InputText = <TFormValues extends Record<string, unknown>>({
  label,
  name,
  register,
  errors,
  className,
  ...props
}: FormInputProps<TFormValues>
) => {
  const errorMessage = errors[name]?.message;
  const isInvalid = !!errorMessage;

  const inputClassName = classNames(
    className,
    {
      "p-invalid": isInvalid,
    });

  return (
    <FieldTemplate
      name={name}
      label={label}
      isInvalid={isInvalid}
      errorMessage={errorMessage as string}
    >
      <PrimeInputText id={name} className={inputClassName} {...register(name)} {...props} />
    </FieldTemplate>
  );
}
