import { Calendar } from 'primereact/calendar';
import { classNames } from "primereact/utils";
import { FormInputProps } from "./form-input-props";
import { FieldTemplate } from "./field-template";

export const InputDate = <TFormValues extends Record<string, unknown>>({
  label,
  name,
  register,
  errors,
  className,
  ...props
}: FormInputProps<TFormValues>
) => {
  const errorMessage = errors[name]?.message;
  const isInvalid = !!errorMessage;

  const inputClassName = classNames(
    className,
    {
      "p-invalid": isInvalid,
    });

  return (
    <FieldTemplate
      name={name}
      label={label}
      isInvalid={isInvalid}
      errorMessage={errorMessage as string}
    >
      <Calendar id={name} className={inputClassName} {...register(name)} {...props} locale="en" />
    </FieldTemplate>
  );
}
