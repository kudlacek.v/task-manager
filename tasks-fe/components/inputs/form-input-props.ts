import { FieldErrors, Path, UseFormRegister } from 'react-hook-form';

export type FormInputProps<TFormValues extends Record<string, unknown>> = {
  label: string;
  name: Path<TFormValues>;
  register: UseFormRegister<TFormValues>;
  errors: FieldErrors<TFormValues>;
  disabled?: boolean;
  className?: string;
};
