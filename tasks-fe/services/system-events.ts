import { EventEmitter } from "tsee";

export const systemEvents = new EventEmitter<{
  'http-error': (userMessage: string, error: Error) => void
}>();
