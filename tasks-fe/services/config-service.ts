type Config = {
  apiUrl: string;
}

class ConfigService {
  private readonly config: Config;

  constructor() {
    this.config = this.getConfig();
  }

  get apiUrl(): string {
    return this.config.apiUrl;
  }

  private getConfig(): Config {
    return {
      apiUrl: this.getString(process.env.NEXT_PUBLIC_API_URL, 'NEXT_PUBLIC_API_URL'),
    }
  }

  private getString(value: string | undefined, name: string): string {
    if (!value) {
      throw new Error(`Missing config value: ${name}`);
    }
    return value;
  }
}

export const configService = new ConfigService();
