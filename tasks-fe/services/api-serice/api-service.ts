'use client';

import { buildQuery, BuildInput } from 'rapiq';
import { configService } from '../config-service';
import { systemEvents } from '../system-events';
import { PaginationModel } from './models/pagination-model';
import { ProjectModel } from './models/project-model';
import { emptyPagination } from './data/empty-pagination';
import { TaskModel } from './models/task-model';

class ApiService {
  constructor(private readonly apiUrl: string) {}

  getProjects = async (requestQuery: BuildInput<ProjectModel>) => {
    const query = buildQuery<ProjectModel>(requestQuery);
    return this.process<PaginationModel<ProjectModel>>(
      'GET',
      '/project' + query,
      undefined,
      emptyPagination<ProjectModel>(),
    );
  }

  createProject = async (project: Omit<ProjectModel, 'id'>): Promise<number> => {
    const response = await this.process<{ id: number }>(
      'POST',
      '/project',
      project,
      { id: 0 },
    );
    return response.id;
  }

  getTasks = async (requestQuery: BuildInput<TaskModel>) => {
    const query = buildQuery<TaskModel>(requestQuery);
    return this.process<PaginationModel<TaskModel>>(
      'GET',
      '/task' + query,
      undefined,
      emptyPagination<TaskModel>(),
    );
  }

  private async process<T>(
    method: 'GET' | 'POST' | 'PUT' | 'DELETE',
    path: string,
    bodyData: unknown,
    defaultResponse: T,
    ): Promise<T> {
    try {
      const result = await fetch(`${this.apiUrl}${path}`, {
        method,
        headers: {
          'Content-Type': 'application/json',
          // TODO: auth token
        },
        body: bodyData ? JSON.stringify(bodyData) : undefined,
      });

      if (result.ok) {
        // TODO: response data validation?
        return result.json() as Promise<T>;
      }

      // TODO: configurable error handling
      systemEvents.emit(
        'http-error',
        'Response has wrong http status',
        new Error(`Wrong http status: ${result.status}`)
      );
      return defaultResponse;
    } catch (error) {
      systemEvents.emit(
        'http-error',
        'Something went wrong during http request.',
        error as Error
      );
      throw error;
    }
  }
}

export const apiService = new ApiService(configService.apiUrl);
