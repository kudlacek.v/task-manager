import { PaginationModel } from "../models/pagination-model";

export function emptyPagination<T>(): PaginationModel<T> {
  return {
    data: [],
    meta: {
      total: 0,
      limit: 0,
      offset: 0,
    },
  };
}
