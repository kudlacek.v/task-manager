export type PaginationModel<T> = {
  data: T[];
  meta: {
    total: number;
    limit: number;
    offset: number;
  }
}
