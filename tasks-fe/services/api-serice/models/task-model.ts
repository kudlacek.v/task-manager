export type TaskModel = {
  id: number;
  name: string;
  description: string;
  status: string; // TODO: enum
  projectId: number;
  // tags: {
  //   id: number;
  //   name: string;
  // }[];
}
