export type ProjectModel = {
  id: number;
  name: string;
  description: string;
}
