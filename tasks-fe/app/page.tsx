"use client";

import { FC } from "react";
import { useRouter } from 'next/navigation'

const Index: FC = () => {
  const router = useRouter();

  router.push('/projects');

  return null;
};

export default Index;
