import { FC } from 'react';
import { ProjectList } from './project-list';

const Page: FC = () => {
  return <ProjectList />;
}

export default Page;
