import { FC } from 'react';
import { ProjectCreate } from './project-create';

const Page: FC = () => {
  return <ProjectCreate />;
}

export default Page;
