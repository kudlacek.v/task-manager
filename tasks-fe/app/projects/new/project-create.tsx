"use client";

import { FC } from "react"
import * as yup from 'yup';
import { useMutation } from 'react-query';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button } from 'primereact/button';
import { useRouter } from 'next/navigation';
import { BlockTitle } from "../../../components/block-title";
import { InputText } from "../../../components/inputs/input-text";
import { InputTextarea } from "../../../components/inputs/input-textarea";
import { InputDate } from "../../../components/inputs/input-date";
import { apiService } from "../../../services/api-serice";

const validation = yup.object({
  name: yup.string().required(),
  description: yup.string().required(),
  startDate: yup.date().required(),
  endDate: yup.date()
    .min(yup.ref('startDate'), 'end date can\'t be before start date')
    .required(),
});

type FormData = yup.InferType<typeof validation>;

export const ProjectCreate: FC = () => {
  const router = useRouter();
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<FormData>({
    defaultValues: {
      name: '',
      description: '',
      startDate: undefined,
      endDate: undefined,
    },
    resolver: yupResolver(validation)
  });
  const { mutate, isLoading } = useMutation(apiService.createProject, {
    onSuccess: (id) => {
      router.push(`/projects/${id}`);
    }
  });

  const onSubmit = (data: FormData) => {
    mutate(data);
  };

  return (
    <>
      <BlockTitle>New project</BlockTitle>
      <form
        onSubmit={handleSubmit(onSubmit)}
        className="p-2"
      >
        <InputText
          label="Name"
          name="name"
          register={register}
          errors={errors}
          disabled={isLoading}
        />
        <InputTextarea
          label="Description"
          name="description"
          register={register}
          errors={errors}
          disabled={isLoading}
        />
        <InputDate
          label="Start Date"
          name="startDate"
          register={register}
          errors={errors}
          disabled={isLoading}
        />
        <InputDate
          label="End Date"
          name="endDate"
          register={register}
          errors={errors}
          disabled={isLoading}
        />
        <Button
          type="submit"
          className="m-2"
          disabled={isLoading}
        >Create Project</Button>
      </form>
    </>
  );
}
