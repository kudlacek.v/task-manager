'use client';

import { FC, useState } from 'react';
import { BuildInput } from 'rapiq';
import { useQuery } from 'react-query';
import { useRouter } from 'next/navigation';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { DataTable, DataTableStateEvent } from 'primereact/datatable';
import { Paginator, PaginatorPageChangeEvent } from 'primereact/paginator';
import { apiService } from '../../services/api-serice';
import { BlockTitle } from '../../components/block-title';
import { ProjectModel } from '../../services/api-serice/models/project-model';
import { toTableSortField, toTableSortOrder } from '../../utils/data-table-utils';

const DEFAULT_PAGE_SIZE = 10;
const DEFAULT_SORT = '-id';

export const ProjectList: FC = () => {
  const router = useRouter();
  const [queryData, setQueryData] = useState<BuildInput<ProjectModel>>({
    pagination: {
      offset: 0,
      limit: DEFAULT_PAGE_SIZE,
    },
    sort: DEFAULT_SORT,
  });
  const { data: paginatedData, isLoading } = useQuery(
    ['projects', queryData],
    () => apiService.getProjects(queryData)
  );

  const onDataTableStateChange = (e: DataTableStateEvent) => {
    // TODO: filters
    setQueryData({
      ...queryData,
      sort: `${e.sortOrder === -1 ? '-' : ''}${
        e.sortField as keyof ProjectModel
      }`,
    });
  };

  const onPageChange = (e: PaginatorPageChangeEvent) => {
    setQueryData({
      ...queryData,
      pagination: {
        offset: e.first,
        limit: e.rows,
      },
    });
  };

  const renderHeader = () => {
    return (
      <div className="flex justify-content-end">
        {/* <span className="p-input-icon-left">
          <i className="pi pi-search" />
          <InputText
            value={globalFilterValue}
            onChange={onGlobalFilterChange}
            placeholder="Fulltext Search"
          />
        </span> */}
      </div>
    );
  };

  const renderActions = (rowData: ProjectModel) => {
    return (
      <Button
        size="small"
        onClick={() => router.push(`/tasks/new/project/${rowData.id}`)}
      >
        New Task
      </Button>
    );
  };

  return (
    <>
      <BlockTitle>Projects</BlockTitle>
      <div>
        TODO: filtering on the server, fulltext search into header
      </div>
      <DataTable
        lazy
        value={paginatedData?.data ?? []}
        loading={isLoading}
        dataKey="id"
        selectionMode="single"
        globalFilterFields={['id', 'name', 'description']}
        header={renderHeader()}
        sortField={toTableSortField<ProjectModel>(queryData.sort)}
        sortOrder={toTableSortOrder<ProjectModel>(queryData.sort)}
        onFilter={onDataTableStateChange}
        onSort={onDataTableStateChange}
        onRowClick={(e) => router.push(`/projects/${e.data.id}`)}
      >
        <Column field="id" header="ID" sortable filter />
        <Column field="name" header="Name" sortable filter />
        <Column field="description" header="Description" sortable filter />
        <Column body={renderActions} />
      </DataTable>
      <Paginator
        first={paginatedData?.meta.offset}
        rows={paginatedData?.meta?.limit}
        totalRecords={paginatedData?.meta?.total}
        onPageChange={onPageChange}
      />
    </>
  );
};
