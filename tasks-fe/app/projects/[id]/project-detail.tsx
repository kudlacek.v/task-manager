"use client";

import { FC } from "react";
import { BlockTitle } from "../../../components/block-title";

type Props = {
  id: number;
}

export const ProjectDetail: FC<Props> = ({ id }) => {
  return (
    <>
      <BlockTitle>Project {id}</BlockTitle>
    </>
  )
}
