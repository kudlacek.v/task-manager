"use client";

import { FC } from 'react';
import { ProjectDetail } from './project-detail';

type Props = {
  params: {
    id: string;
  }
}

const DetailPage: FC<Props> = ({ params }) => {
  // TODO: try parse id
  return <ProjectDetail id={parseInt(params.id)} />;
}

export default DetailPage;
