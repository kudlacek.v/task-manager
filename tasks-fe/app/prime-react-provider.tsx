"use client";

import { FC, ReactNode } from "react";
import { PrimeReactProvider as Original } from 'primereact/api';

type Props = {
  children: ReactNode;
}

export const PrimeReactProvider: FC<Props> = ({ children }) => {
  return <Original>{children}</Original>
};
