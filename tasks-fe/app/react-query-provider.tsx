"use client";

import { FC, ReactNode } from "react";
import { QueryClient, QueryClientProvider } from "react-query";

type Props = {
  children: ReactNode;
}

const queryClient = new QueryClient();

export const ReactQueryProvider: FC<Props> = ({ children }) => {
  // TODO: configure react-query according to application requirements
  return (
    <QueryClientProvider client={queryClient}>
      {children}
    </QueryClientProvider>
  );
}
