"use client";

import { FC, useState } from "react";
import { BuildInput } from 'rapiq';
import { useQuery } from 'react-query';
import { useRouter } from 'next/navigation';
import { Column } from 'primereact/column';
import { DataTable, DataTableStateEvent } from 'primereact/datatable';
import { Paginator, PaginatorPageChangeEvent } from 'primereact/paginator';
import { apiService } from '../../services/api-serice';
import { BlockTitle } from "../../components/block-title";
import { TaskModel } from "../../services/api-serice/models/task-model";
import { toTableSortField, toTableSortOrder } from "../../utils/data-table-utils";

const DEFAULT_PAGE_SIZE = 10;
const DEFAULT_SORT = '-id';

export const TaskList: FC = () => {
  const router = useRouter();
  const [queryData, setQueryData] = useState<BuildInput<TaskModel>>({
    pagination: {
      offset: 0,
      limit: DEFAULT_PAGE_SIZE,
    },
    sort: DEFAULT_SORT,
  });
  const { data: paginatedData, isLoading } = useQuery(
    ['tasks', queryData],
    () => apiService.getTasks(queryData)
  );

  const onDataTableStateChange = (e: DataTableStateEvent) => {
    // TODO: filters
    setQueryData({
      ...queryData,
      sort: `${e.sortOrder === -1 ? '-' : ''}${
        e.sortField as keyof TaskModel
      }`,
    });
  };

  const onPageChange = (e: PaginatorPageChangeEvent) => {
    setQueryData({
      ...queryData,
      pagination: {
        offset: e.first,
        limit: e.rows,
      },
    });
  };

  return (
    <>
      <BlockTitle>Tasks</BlockTitle>
      <div>
        TODO: filtering on the server, fulltext search into header
      </div>
      <DataTable
        lazy
        value={paginatedData?.data ?? []}
        loading={isLoading}
        dataKey="id"
        selectionMode="single"
        globalFilterFields={['id', 'name', 'description']}
        sortField={toTableSortField<TaskModel>(queryData.sort)}
        sortOrder={toTableSortOrder<TaskModel>(queryData.sort)}
        onFilter={onDataTableStateChange}
        onSort={onDataTableStateChange}
        onRowClick={(e) => router.push(`/tasks/${e.data.id}`)}
      >
        <Column field="id" header="ID" sortable filter />
        <Column field="name" header="Name" sortable filter />
        <Column field="description" header="Description" sortable filter />
      </DataTable>
      <Paginator
        first={paginatedData?.meta.offset}
        rows={paginatedData?.meta?.limit}
        totalRecords={paginatedData?.meta?.total}
        onPageChange={onPageChange}
      />
    </>
  )
}
