import { FC } from 'react';
import { TaskList } from './task-list';

const Page: FC = () => {
  return <TaskList />;
};

export default Page;
