import { FC } from "react"
import { BlockTitle } from "../../../../components/block-title"

export const TaskCreate: FC = () => {
  return (
    <>
      <BlockTitle>Create Task</BlockTitle>
    </>
  );
}
