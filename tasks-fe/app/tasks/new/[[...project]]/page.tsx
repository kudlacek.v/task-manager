import { FC } from 'react';
import { TaskCreate } from './task-create';

const Page: FC = () => {
  return <TaskCreate />;
}

export default Page;
