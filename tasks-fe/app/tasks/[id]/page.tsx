"use client";

import { FC } from 'react';
import { TaskDetail } from './task-detail';

type Props = {
  params: {
    id: string;
  }
}

const Page: FC<Props> = ({ params }) => {
  // TODO: try parse id
  return <TaskDetail id={parseInt(params.id)} />;
}

export default Page;
