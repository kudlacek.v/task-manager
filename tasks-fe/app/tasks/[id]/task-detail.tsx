"use client";

import { FC } from "react";
import { BlockTitle } from "../../../components/block-title"

type Props = {
  id: number;
}

export const TaskDetail: FC<Props> = ({ id }) => {
  return (
    <>
      <BlockTitle>Task {id}</BlockTitle>
    </>
  )
}
