# In this repo
This repository contains an application developed as a demonstration of Nx Monorepo, Next.js (React), Tailwind CSS, NestJS, TypeORM and PostgreSQL technologies.

### Requirements
PostgreSQL database instance.

### Configuration
Create copy of file ./tasks-be/.env to location ./tasks-be/.env.local and configure variables for PostgreSQL database connection.

### Development
All commands described below run in project root folder.
- Start with `yarn install`.
- If you start project for the first time apply database migrations ([Database Migration](#database-migrations)).
- To start backend app in watch mode run `yarn serve-be`.
- To start frontend app in watch mode run `yarn serve-fe`.

### Frontend
Frontend is Next.js application with output export. It can be hosted on CDN.
You can edit source code in `./tasks-fe` folder.

### Backend
Backend is NestJS application.
The source code is in `./tasks-be` folder.
There is OpenAPI documentation by swagger on `http://localhost:3000/api/swagger`. Api is not fully documented yet.

### Database migrations
- Generate migration `yarn typeorm migration:generate -d ./tasks-be/src/common/config/database.config.ts ./tasks-be/migrations/{short_migration_description}`.
- Apply migration `yarn typeorm migration:run -d ./tasks-be/src/common/config/database.config.ts`.


## Notes
- There are countless possible improvements. Both the application itself and the code and development method.
- Given the time available, I made a lot of compromises. For example, there are no unit tests or integration tests, because the project will not be developed in the long term.
- I used Nx Monorepo to test the functionality, but I haven't found any major advantages to this type of project yet.
